import os
import discord
from discord.ext import commands
import yt_dlp as youtube_dl


intents = discord.Intents.all()
intents.members = True

client = commands.Bot(command_prefix='!', intents=intents)

@client.event
async def on_ready():
    print('Bot is ready.')


@client.command()
async def play(ctx, url):
    song_there = os.path.isfile("song.mp3")
    try:
        if song_there:
            os.remove("song.mp3")
    except PermissionError:
        em8 = discord.Embed(title = "Music Is Currently Playing", description = 'Please wait for the current playing music.',color = ctx.author.color)
        await ctx.send(embed = em8)
        return

    voiceChannel = discord.utils.get(ctx.guild.voice_channels)
    await voiceChannel.connect()
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    em6 = discord.Embed(title = "Downloading Youtube Music", description = f'{url}\n\nPlease wait to setup the music you provide.',color = ctx.author.color)
    await ctx.send(embed = em6, delete_after = 2)
    await ctx.message.delete()

    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '196',
        }],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])
    for file in os.listdir("./"):
        if file.endswith(".mp3"):
            os.rename(file, "song.mp3")
    voice.play(discord.FFmpegPCMAudio("song.mp3"))
    em1 = discord.Embed(title = "Now Listening Youtube Music", description = f'{url}\n\nPlaying.',color = ctx.author.color)

    videoID = url.split("watch?v=")[1].split("&")[0]

    em1.set_thumbnail(url = f'https://img.youtube.com/vi/{videoID}/default.jpg'.format(videoID = videoID))
    await ctx.send(embed = em1)

@client.event
async def on_message(message):
    print("Message received: " + message.content)
    if message.content == '!hello' and message.author != client.user:
        print("Sending hello message")
        await message.channel.send("Hello, " + message.author.name + "!")
    elif message.content == '!help' and message.author != client.user:
        print("Sending help message")
        await message.channel.send("Hi, " + message.author.name + "! I am a bot that can play music from youtube. To use me, type !play followed by the youtube link of the song you want to play. For example, !play https://www.youtube.com/watch?v=dQw4w9WgXcQ")
    else:
        await client.process_commands(message)

token = open('/home/adam/a_bot_token').read().strip()
client.run(token)
